Minimal reproduction repo for https://github.com/facebook/jest/issues/11398

To run:

```sh
yarn && yarn test
```
